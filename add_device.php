<?php
require ('fonction.php');
session_start();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
  <link rel="stylesheet" href="style.css" />
  <title>Ajouter un Equipement</title>
</head>
<body>
  <?php
  if (!empty($_SESSION['logged_in']))
  {
    $ip_address = htmlspecialchars($_POST['ip_address']);
    $username = htmlspecialchars($_POST['username_device']);
    $password = htmlspecialchars($_POST['password_device']);
    $password_enable = htmlspecialchars($_POST['password_enable']);
    $modele = htmlspecialchars($_POST['modele']);
    $name = htmlspecialchars($_POST['name']);
    $connection = htmlspecialchars($_POST['connection']);
    $auto_enable = htmlspecialchars($_POST['auto_enable']);
    $modele = trim($modele);  // On nettoie la variable car il y a un retour chariot à la fin

    if(filter_var($ip_address, FILTER_VALIDATE_IP)) // Vérification du format adresse IP
    {
        bdConnection();
        $insert = $GLOBALS['bd']->prepare("INSERT INTO devices(ip_address, name, username, password, auto_enable, password_enable, connection, modele) VALUES(:ip_address, :name, :username, :password, :auto_enable, :password_enable, :connection, :modele)");
        // Liaison des paramètres reçus via le formulaire avec la BDD et execution de la requête
        $insert->bindParam(':ip_address', $ip_address);
        $insert->bindParam(':name', $name);
        $insert->bindParam(':username', $username);
        $insert->bindParam(':password', $password);
        $insert->bindParam(':auto_enable', $auto_enable);
        $insert->bindParam(':password_enable', $password_enable);
        $insert->bindParam(':connection', $connection);
        $insert->bindParam(':modele', $modele);
        $insert->execute();
      {
        header('location:erreur.php');
      }
    }
    else {
      echo "Le format IP n'est pas respecté";
    }
    header('Location:index.php');
  }
  else
  {
    header('Location:login.php');
  }
 ?>
