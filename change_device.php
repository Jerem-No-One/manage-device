<?php
require ('fonction.php');
session_start();
if (!empty($_SESSION['logged_in']))
{
  $ip_address = htmlspecialchars($_POST['ip_address']);
  $username = htmlspecialchars($_POST['username_device']);
  $password = htmlspecialchars($_POST['password_device']);
  $password_enable = htmlspecialchars($_POST['password_enable']);
  $modele = htmlspecialchars($_POST['modele']);
  $name = htmlspecialchars($_POST['name']);
  $connection = htmlspecialchars($_POST['connection']);
  $auto_enable = htmlspecialchars($_POST['auto_enable']);
  $id=htmlspecialchars($_POST['id']);
  $modele = trim($modele);  // On nettoie la variable car il y a un retour chariot à la fin

  if(filter_var($ip_address, FILTER_VALIDATE_IP)) // Vérification du format adresse IP
  {
    bdConnection();
    $R = $GLOBALS['bd']->query("UPDATE devices SET ip_address='$ip_address', username='$username', password='$password', password_enable='$password_enable', modele='$modele', name='$name', connection='$connection', auto_enable='$auto_enable' WHERE id='$id'");
    header('Location:index.php'); // Modification du device passé en paramètre
  }
  else {
    echo "Le format IP n'est pas respecté";
  }
}
else
{
  header('Location:login.php');
}

?>
