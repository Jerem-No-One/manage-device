function passwordEnable() // Fonction permettant d'activer le champs password enable en fonction de la case cochée
{
  if(document.getElementById('non').checked == true)
  {
    document.getElementById('disabledInput').disabled = false;
  }
  else if (document.getElementById('oui').checked == true)
  {
    document.getElementById('disabledInput').disabled = true;
  }
}

function deleteDevice() // Confirmation de suppression
{
  if(confirm("Voulez-vous vraiment supprimer ?"))
  {
    delete_device.submit();
  }
}

// if(document.location.protocol == "http:")
// {
//   document.location.href = "https://"+document.location.host+document.location.pathname;
// }
