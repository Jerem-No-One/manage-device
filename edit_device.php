<?php
session_start();
require ('fonction.php');
?>
<html>
<head>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
  <link rel="stylesheet" href="style.css" />
  <title>Rancid - Edit</title>
</head>
<body>
  <?php
  if (!empty($_SESSION['logged_in']))
  {
    $id=htmlspecialchars($_POST['id']);
    ?>
    <div class="container">
      <div class="raw">
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php">Rancid - Manage</a>
            </div>
            <div class="collapse navbar-collapse" id="#myNavbar">
              <ul class="nav navbar-nav navbar-right">
                <!-- <li><a href="websvn/index.php"><span class="glyphicon glyphicon-list"></span> Websvn</a></li> -->
                <li><a href="settings.php"><span class="glyphicon glyphicon-cog"></span> Paramètres</a></li>
                <li><a href="logout.php"><span class="glyphicon glyphicon-off"></span> Déconnexion</a></li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
    <div class="container">
      <form  action="change_device.php" method="post" name="change_device" class="form">  <!--Formulaire ajout d'un device-->
        <?php
        bdConnection();
        $R = $GLOBALS['bd']->query("SELECT * FROM devices WHERE ID='$id'");
        while ($donnees1 = $R->fetch())
        {
          ?>
          <legend class="text-center">Modification de l'équipement</legend>
          <div class="row">
            <div>
              <input type="hidden" name="id" value="<?php echo $donnees1['ID']; ?>"</input>
            </div>
            <div class="col-md-offset-1 col-md-4">
              <div class="form-group">
                <label class="control-label" for="ip_address">Adresse IP</label>
                <input type="text" class="form-control" value="<?php echo $donnees1['ip_address']; ?>" name="ip_address" autofocus=""/>
              </div>
            </div>
            <div class="col-md-offset-2 col-md-4">
              <div class="form-group">
                <label class="control-label" for="name">Nom de l'équipement</label>
                <input type="text" class="form-control" name="name" value="<?php echo $donnees1['name'];?>"/>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-offset-1 col-md-4">
              <div class="form-group">
                <label class="control-label" for="authentication">Authentification</label>
                <input type="text" class="form-control" name="username_device" value="<?php echo $donnees1['username'];?>"/>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-offset-2 col-md-4">
                <label class="control-label" for="password">Mot de passe ssh/telnet</label>
                <input type="password" class="form-control" name="password_device" value="<?php echo $donnees1['password'];?>"/>
              </div>
            </div>
          </div>
          <div class="row">
            <?php
            if($donnees1['auto_enable']=='oui')
            {
              ?>
              <div class="col-md-offset-1 col-md-4">
                <div class="form-group">
                  <label for="auto_enable">Auto-enable :</label>
                  <input type="radio" class="form-control radio-inline" name="auto_enable" value="oui" id="oui" checked="checked" onclick="passwordEnable()"/>
                  <label for="oui" class="text-radio">Oui</label>
                  <input type="radio" class="form-control radio-inline" name="auto_enable" value="non" id="non" onclick="passwordEnable()"/>
                  <label for="non" class="text-radio">Non</label>
                </div>
              </div>
              <div class="col-md-offset-2 col-md-4">
                <div id="password_enable" class="form-group">
                  <label class="control-label" for="password_enable">Mot de passe enable</label>
                  <input id="disabledInput" class="form-control" type="password" name="password_enable" value="<?php echo $donnees1['password_enable'];?>" disabled/>
                </div>
              </div>
              <?php
            }
            else{
              ?>
              <div class="col-md-offset-1 col-md-4">
                <div class="form-group">
                  <label for="auto_enable">Auto-enable :</label>
                  <input type="radio" class="form-control radio-inline" name="auto_enable" value="oui" id="oui"  onclick="passwordEnable()"/>
                  <label for="oui" class="text-radio">Oui</label>
                  <input type="radio" class="form-control radio-inline" name="auto_enable" value="non" id="non" checked="checked" onclick="passwordEnable()"/>
                  <label for="non" class="text-radio">Non</label>
                </div>
              </div>
              <div class="col-md-offset-2 col-md-4">
                <div id="password_enable" class="form-group">
                  <label class="control-label" for="password_enable">Mot de passe enable</label>
                  <input id="disabledInput" class="form-control" type="password" name="password_enable" value="<?php echo $donnees1['password_enable'];?>"/>
                </div>
              </div>
            <?php } ?>
            <div class="row">
              <?php
              if($donnees1['connection'] == 'ssh')
              {
                ?>
                <div class="col-md-offset-1 col-md-4">
                  <div class="form-group">
                    <label for="connection">Connectivité :</label>
                    <input type="radio" class="form-control radio-inline" name="connection" value="ssh" id="ssh" checked="checked"/>
                    <label for="ssh" class="text-radio">SSH</label>
                    <input type="radio" class="form-control radio-inline" name="connection" value="telnet" id="telnet"/>
                    <label for="telnet" class="text-radio">Telnet</label>
                  </div>
                </div>
                <?php
              }
              else
              {
                ?>
                <div class="col-md-offset-1 col-md-4">
                  <div class="form-group">
                    <label for="connection">Connectivité :</label>
                    <input type="radio" class="form-control radio-inline" name="connection" value="ssh" id="ssh" checked="checked"/>
                    <label for="ssh" class="text-radio">SSH</label>
                    <input type="radio" class="form-control radio-inline" name="connection" value="telnet" id="telnet"/>
                    <label for="telnet" class="text-radio">Telnet</label>
                  </div>
                </div>
              <?php } ?>
              <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                  <label class="control-label" for="modele">Modèle d'équipement</label>
                  <select name="modele" class="form-control"> <!-- Select dynamique-->
                    <?php
                    bdConnection();
                    $R = $GLOBALS['bd']->query("SELECT * FROM modeles");
                    while ($donnees = $R->fetch())
                    {
                      echo '<option value="'.$donnees['name'].'">'.$donnees['name'].'</option>';
                    }
                }
              
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-md-offset-4 text-center">
                <div class="btn-group" role="group" aria-label="Basic example">
                  <button type="button" class="btn btn-default btn-sm" onclick="location.href='index.php'">
                    <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
                  </button>
                  <button type="submit" class="btn btn-default btn-sm center-block">
                    <span class="glyphicon glyphicon-ok"></span> Valider
                  </button>
                </div>
              </div>
            </div>
          </div>
      </div>
    </form>
    <footer>
      <div class="container">
        <div class="row">
          <p>&copy; 2017 B2M SAS</p>
        </div>
      </div>
    </div>
  </footer>
  <?php
  }
  else
  {
  header('Location:login.php');
  }
  ?>
<script src="code.js"></script>
</body>
</html>
