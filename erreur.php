<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
  <link rel="stylesheet" href="style.css" />
  <script src="jquery-3.2.1.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <title>Rancid - Erreur</title>
</head>
<body>
  <?php
  if (!empty($_SESSION['logged_in']))
  {
    ?>
    <div class="container">
      <div class="row">
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php">Rancid - Erreur</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="websvn/index.php"><span class="glyphicon glyphicon-list"></span> Websvn</a></li>
                <li><a href="settings.php"><span class="glyphicon glyphicon-cog"></span> Paramètres</a></li>
                <li><a href="logout.php"><span class="glyphicon glyphicon-off"></span> Déconnexion</a></li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
    <div class="container">
      <div class="text-center">
        <br></br>
        <br></br>
        <legend> Erreur ... </legend>
        <p> L'adresse IP ou le nom est déjà affecté à un équipement. </p>
        <button type="button" class="btn btn-default btn-sm" onclick="location.href='index.php'">
          <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
        </button>
      </div>
      <footer>
        <div class="container">
          <div class="row">
            <p>&copy; 2017 B2M SAS</p>
          </div>
        </div>
      </div>
    </footer>
    <?php
  }
  else
  {
    header('Location:login.php');
  }
  ?>
  <script src="code.js"></script>
</body>
</html>
