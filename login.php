<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
  <link rel="stylesheet" href="style.css" />
  <title>Login</title>
</head>
<script type="text/javascript" src="code.js"></script>
<body>
  <?php
  require ('db.php');
  require ('fonction.php');
  session_start();
  if (!empty($_POST))
  { // Envoi de l'authentification si
    $username = htmlspecialchars($_POST['$username']);
    $password = htmlspecialchars($_POST['$password']);
    bdConnection();
    $R = $GLOBALS['bd']->query("SELECT * FROM user");
    $password_hash = sha1($_POST['password']);
    while ($donnees = $R->fetch())
    {
    if ($password_hash === $donnees['password'] && $_POST['username'] === $donnees['username']) // Envoi de l'authentification si l'utilisateur n'est pas identifié
    {
      $_SESSION['logged_in'] = true;
      header('Location:index.php');
    }
    else{
      header('location:login.php');
    }
  }
  }
    else
    {
      ?>
      <form method="post">
        <div class="container">
          <div class="row">
            <div class="text-center">
              <h1>Authentification</h1>
            </div>
          </div>
          <div class="row">
            <div class="text-center">
              <input type="text" name="username" placeholder="Username" autofocus="" required=""/>
            </div>
          </div>
          <div class="row">
            <div class="text-center">
              <input type="password" name="password" placeholder="Password" required=""/>
            </div>
          </div>
          <div class="row">
            <div class="text-center">
              <input type="submit" value="Connexion" class="btn btn-default btn-sm"/>
            </div>
          </div>
        </div>
      </form>
      <?php
    }
    ?>
  </body>
  </html>
