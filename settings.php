<?php
session_start();
require ('fonction.php');
?>
<html>
<head>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="bootstrap/css/bootstrap.css" />
  <link rel="stylesheet" href="style.css" />
  <title>Rancid - Settings</title>
</head>
<body>
  <?php
  if (!empty($_SESSION['logged_in']))
  {
    $hostname = shell_exec("cat /etc/hostname");
    bdConnection();
    $R = $GLOBALS['bd']->query("SELECT * FROM user");
    while ($donnees = $R->fetch())
    {
      $username = $donnees['username'];
      $password = $donnees['password'];
    }
    ?>
    <div class="container">
      <div class="raw">
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php">Rancid - Manage</a>
            </div>
            <div class="collapse navbar-collapse" id="#myNavbar">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="settings.php"><span class="glyphicon glyphicon-cog"></span> Paramètres</a></li>
                <li><a href="logout.php"><span class="glyphicon glyphicon-off"></span> Déconnexion</a></li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
    <div class="container">
    <form  action="change_hostname.php" method="post" name="change_hostname" class="form">
      <legend class="text-center">Hostname</legend>
        <div class="row">
          <div class="text-center">
            <input class ="center-block" type="text" name="hostname" value="<?php echo $hostname ?>" required="">
          </div>
        </div>
        <div class="row">
          <div class="text-center">
            <div class="btn-group" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-default btn-sm" onclick="location.href='index.php'">
                <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
              </button>
              <button type="submit" class="btn btn-default btn-sm">
                <span class="glyphicon glyphicon-edit"></span> Modifier
              </button>
            </div>
          </div>
        </div>
      </form>
      <form  action="change_user.php" method="post" name="change_user" class="form">
        <legend class="text-center">Change Password</legend>
          <div class="row"> 
            <div class="text-center">
              <input class ="center-block" type="password" name="password1" placeholder="New Password" required="">
            </div>
            <div class="text-center">
              <input type="password" name="password2" placeholder="Retype Password" required="">
            </div>
          </div>
          <div class="row">
            <div class="text-center">
              <div class="btn-group" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-default btn-sm" onclick="location.href='index.php'">
                  <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
                </button>
                <button type="submit" class="btn btn-default btn-sm">
                  <span class="glyphicon glyphicon-edit"></span> Modifier
                </button>
              </div>
            </div>
          </div>
        </form>
      <footer>
        <div class="container">
          <div class="row">
            <p>&copy; 2017 B2M SAS</p>
          </div>
        </div>
      </div>
    </footer>
    <?php
  }
  else
  {
    header('Location:login.php');
  }
  ?>
</body>
</html>
